import React, { Component } from 'react'
import PropTypes from 'prop-types'

class EditableValue extends React.Component {
  
  constructor(props) {
    super(props)
    this.state = {
      currentValue: props.itemValue,
      isEditing: false,
      isFocused: false,
    }
    this.onSelectItemValue = this.onSelectItemValue.bind(this)
    this.onCommitChange = this.onCommitChange.bind(this)
    this.onChangeValue = this.onChangeValue.bind(this)
  }
  
  componentDidUpdate() {
    if (this.state.isEditing && !this.state.isFocused) {
      this.inputField.select()
      this.setState({
        isFocused: true
      })
    }
  }
  
  onSelectItemValue() {
    this.setState({
      isEditing: true
    })
  }
  
  onCommitChange() {
    this.setState({
      isEditing: false,
      isFocused: false,
    })
    this.props.onValueChange(
      this.props.itemIdentifier, 
      this.state.currentValue
    )
  }
  
  onChangeValue(evnt) {
    this.setState({currentValue: evnt.target.value})
  }
  
  render() {
    const type = this.state.isEditing ? 'type' : 'hidden'
    return <span 
             className='react-editable-list-item-value'
             onClick={this.onSelectItemValue} 
             >
               <input 
                 type={type} 
                 onBlur={this.onCommitChange} 
                 onChange={this.onChangeValue}
                 ref={(input) => { this.inputField = input } }
                 value={ this.state.currentValue }
                 />
               { !this.state.isEditing && this.state.currentValue }
           </span>
  }
}

EditableValue.propTypes = {
  itemIdentifier: PropTypes.string.isRequired,
  itemValue: PropTypes.string.isRequired,
  onValueChange: PropTypes.func.isRequired,
}

export default EditableValue