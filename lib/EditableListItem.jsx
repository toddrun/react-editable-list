import React, { Component } from 'react'
import PropTypes from 'prop-types'

import EditableValue from './EditableValue.jsx'
import DeleteItemButton from './DeleteItemButton.jsx'

class EditableListItem extends React.Component {
  render() {
    return <li className='react-editable-list-item'>
      <EditableValue {...this.props} />
      <DeleteItemButton {...this.props} onDeleted={this.props.onValueRemoved} />
    </li>
  }
}

EditableListItem.propTypes = {
  itemIdentifier: PropTypes.string.isRequired,
  itemValue: PropTypes.string.isRequired,
  noUp: PropTypes.bool,
  noDown: PropTypes.bool,
  onValueChange: PropTypes.func.isRequired,
  onValueRemoved: PropTypes.func.isRequired,
}

export default EditableListItem