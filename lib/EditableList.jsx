import React, { Component } from 'react'
import PropTypes from 'prop-types'

import EditableListItem from './EditableListItem.jsx'
import AddItemButton from './AddItemButton.jsx'

class EditableList extends React.Component {
  
  constructor(props) {
    super(props)
    const buildInitialList = (list) => {
      return list.map((listItem) => {
        return {itemValue: listItem, itemIdentifier: 'i_' + listItem}
      })
    }
    this.state = {
      currentList: buildInitialList(this.props.list)
    }
    this.onValueChange = this.onValueChange.bind(this)
    this.onValueRemoved = this.onValueRemoved.bind(this)
    this.onNewValue = this.onNewValue.bind(this)
    this.listValues = this.listValues.bind(this)
  }
  
  onValueChange(identifier, value) {
    const buildUpdatedList = (identifier, value) => {
      return this.state.currentList.map((element) => {
        if (element.itemIdentifier == identifier) {
          return {itemIdentifier: identifier, itemValue: value}
        }
        return {...element}
      })
    }
    const newList = buildUpdatedList(identifier, value)
    this.setState({
      currentList: newList
    })
    this.props.onListUpdated(this.listValues(newList))
  }
  
  onValueRemoved(identifier) {
    const reducedList = this.state.currentList.filter(item => item.itemIdentifier != identifier)
    this.setState({
      currentList: reducedList
    })
    this.props.onListUpdated(this.listValues(reducedList))
    
  }
  
  onNewValue(identifier) {
    const expandedList = this.state.currentList.slice()
    const label = this.props.newItemLabel || 'new item'
    expandedList.push({itemIdentifier: identifier, itemValue: label})
    this.setState({
      currentList: expandedList
    })
  }
  
  listValues(list) {
    return list.map(item => item.itemValue)
  }
  
  render() {
    const currentList = this.state.currentList
    return <ul className='react-editable-list'>
      { currentList.map((item, idx) => {
          return <EditableListItem 
                   key={item.itemIdentifier} 
                   itemIdentifier={item.itemIdentifier}
                   itemValue={item.itemValue} 
                   noUp={idx === 0}
                   noDown={idx === currentList.length - 1}
                   onValueChange={this.onValueChange}
                   onValueRemoved={this.onValueRemoved}
                   {...this.props}
                   />                  
      })}
      <AddItemButton onAdd={this.onNewValue} />
    </ul>
  }
}

EditableList.propTypes = {
  list: PropTypes.arrayOf(PropTypes.string).isRequired,
  onListUpdated: PropTypes.func.isRequired,
}

export default EditableList