import React, { Component } from 'react'
import PropTypes from 'prop-types'

class AddItemButton extends React.Component {
  constructor(props) {
    super(props)
    this.index = 0
    this.onAddPress = this.onAddPress.bind(this)
  }
  
  onAddPress() {
    const newLabel = 'idx_' + this.index + '_' + new Date().getTime()
    this.props.onAdd(newLabel)
    this.index = this.index + 1
  }
  
  render() {
    return <button 
             className='react-editable-list-add-button'
             onClick={this.onAddPress}
           >
           { this.props.buttonLabel || 'add' }
           </button>
  }
}

AddItemButton.propTypes = {
  onAdd: PropTypes.func.isRequired,
  buttonLabel: PropTypes.string,
}

export default AddItemButton