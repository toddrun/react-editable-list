import React, { Component } from 'react'
import PropTypes from 'prop-types'

class DeleteItemButton extends React.Component {
  constructor(props) {
    super(props)
    this.onDeletePress = this.onDeletePress.bind(this)
  }
  
  onDeletePress() {
    this.props.onDeleted(this.props.itemIdentifier)
  }
  
  render() {
    return <button 
             className='react-editable-list-delete-button'
             onClick={this.onDeletePress}
           >
           { this.props.buttonLabel || 'delete' }
           </button>
  }
}

DeleteItemButton.propTypes = {
  itemIdentifier: PropTypes.string.isRequired,
  onDeleted: PropTypes.func.isRequired,
  buttonLabel: PropTypes.string,
}

export default DeleteItemButton