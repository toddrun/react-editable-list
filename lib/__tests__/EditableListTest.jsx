import React from 'react'
import { expect } from 'chai'
import { shallow } from 'enzyme'
import sinon from 'sinon'

import EditableList from '../EditableList.jsx'

describe ('EditableList', () => {
  const list = ['one', 'two', 'three']
  const onListUpdatedCallback = sinon.spy()
  const component = shallow(<EditableList 
                              list={list} 
                              onListUpdated={onListUpdatedCallback} 
                              />
                           )
                                      
  it ('is wrapped in a "ul" element', () => {
    expect(component.find('ul')).to.have.length(1)
  })
  
  it ('set a "react-editable-list" className on the "ul"', () => {
    expect(component.find('ul').hasClass('react-editable-list')).to.be.true
  })
  
  it ('includes an EditableListItem for each list entry', () => {
    expect(component.find('EditableListItem')).to.have.length(3)
  })
  
  it ('includes a single AddItemButton', () => {
    expect(component.find('AddItemButton')).to.have.length(1)
  })
  
  it ('applies one list value to each of the EditableListItems', () => {
    list.map((itemValue) => {
      const listItem = component.findWhere((element) => {
        return element.prop('itemValue') == itemValue
      })
      expect(listItem.exists()).to.be.true
      expect(listItem.prop('itemIdentifier')).to.eq('i_' + itemValue)
      expect(listItem.key()).to.eq(listItem.prop('itemIdentifier'))
    })
  })
  
  it ('passes "noUp" on first EditableListItem', () => {
    const item = component.find('EditableListItem').first()
    expect(item.prop('noUp')).to.be.true
    expect(item.prop('noDown')).to.be.false
  })
  
  it ('passes "noDown" on last EditableListItem', () => {
    const item = component.find('EditableListItem').last()
    expect(item.prop('noUp')).to.be.false
    expect(item.prop('noDown')).to.be.true
  })
  
  it ('does not pass "noUp" or "noDown" on inner EditableListItems', () => {
    const item = component.find('EditableListItem').at(1)
    expect(item.prop('noUp')).to.be.false
    expect(item.prop('noDown')).to.be.false
  })
  
  it ('passes onValueChange and onValueRemoved to each of the EditableListItems', () => {
    component.children().forEach((child) => {
      if (child.type == 'EditableListItem') {
        expect(child.prop('onValueChange')).to.not.be.null
        expect(child.prop('onValueChange')).to.eq(component.instance().onValueChange)
        expect(child.prop('onValueRemoved')).to.not.be.null
        expect(child.prop('onValueRemoved')).to.eq(component.instance().onValueRemoved)
      }
    })
  })
  
  it ('passes onNewValue to AddItemButton', () => {
    expect(component.find('AddItemButton').prop('onAdd')).to.eq(component.instance().onNewValue)
  })
  
  it ('calls "onListUpdated" prop with updated list when a child calls "onValueChange"', () => {
    component.instance().onValueChange('i_one', 'ones')
    expect(onListUpdatedCallback.calledWith(['ones', 'two', 'three'])).to.be.true
  })
  
  it ('calls "onListUpdated" prop with updated list when "onValueRemoved" is called', () => {
    component.instance().onValueRemoved('i_two')
    expect(onListUpdatedCallback.calledWith(['ones', 'three'])).to.be.true
  })
  
  it ('does not call "onListUpdated" prop when "onNewValue" is called', () => {
    const newSpy = sinon.spy()
    const freshComponent = shallow(<EditableList 
                                     list={list} 
                                     onListUpdated={newSpy} 
                                     />
                                  )
    freshComponent.instance().onNewValue('some_id')
    expect(newSpy.called).to.be.false
  })
  
  it ('adds "new item" to internal list when "onNewValue" is called', () => {
    component.instance().onNewValue('my_new_item')
    const newItem = component.state('currentList').find(item => item.itemIdentifier == 'my_new_item')
    expect(newItem.itemValue).to.eq('new item')
  })
  
  it ('allows overriding "new item" with another label', () => {
    const specificLabel = shallow(<EditableList 
                                     list={list} 
                                     onListUpdated={onListUpdatedCallback} 
                                     newItemLabel='fresh entry'
                                     />
                                  )
    specificLabel.instance().onNewValue('my_new_item')
    const newItem = specificLabel.state('currentList').find(item => item.itemIdentifier == 'my_new_item')
    expect(newItem.itemValue).to.eq('fresh entry')
  })
})