import React from 'react'
import { expect } from 'chai'
import { shallow } from 'enzyme'

import EditableListItem from '../EditableListItem.jsx'

describe ('EditableListItem', () => {
  const identifier = 'x'
  const value = 'y'
  const onValueChangeCallback = () => {}
  const onValueRemovedCallback = () => {}
  const component = shallow(<EditableListItem 
                              itemIdentifier={identifier}
                              itemValue={value}
                              onValueChange={onValueChangeCallback}
                              onValueRemoved={onValueRemovedCallback}
                              />
                           )
                                      
  it ('is wrapped in a "li" element', () => {
    expect(component.find('li')).to.have.length(1)
  })
  
  it ('set a "react-editable-list-item" className on the "li"', () => {
    expect(component.find('li').hasClass('react-editable-list-item')).to.be.true
  })
  
  it ('includes an "EditableValue" with the item', () => {
    expect(component.find('EditableValue')).to.have.length(1)
    expect(component.find('EditableValue').prop('itemValue')).to.eq(value)
  })
  
  it ('includes a "DeleteItemButton" with the itemIdentifier', () => {
    expect(component.find('DeleteItemButton')).to.have.length(1)
    expect(component.find('DeleteItemButton').prop('itemIdentifier')).to.eq(identifier)
  })
  
  it ('passes the event callbacks to child components', () => {
    expect(component.find('EditableValue').prop('onValueChange')).to.eq(onValueChangeCallback)
    expect(component.find('DeleteItemButton').prop('onValueRemoved')).to.eq(onValueRemovedCallback)
  })
})