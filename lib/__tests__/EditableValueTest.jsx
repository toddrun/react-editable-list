import React from 'react'
import { expect } from 'chai'
import { shallow } from 'enzyme'
import sinon from 'sinon'

import EditableValue from '../EditableValue.jsx'

describe ('EditableValue', () => {
  const identifier = 'i_one'
  const itemValue = 'one'
  const callback = sinon.spy().withArgs(identifier, itemValue)
  const component = shallow(<EditableValue 
                              itemIdentifier={identifier}
                              itemValue={itemValue}
                              onValueChange={callback} 
                              />
                           )
                                      
  it ('wraps the itemValue in a span', () => {
    expect(component.find('span')).to.have.length(1)
    expect(component.text()).to.eq('one')
  })
                                     
  it ('sets a "react-editable-list-item-value" class on the span', () => {
    expect(component.find('span').hasClass('react-editable-list-item-value')).to.be.true
  })
  
  it ('calls onValueChanged when blurred', () => {
    const componentInstance = component.instance()
    component.find('span').simulate('click')
    component.find('input').simulate('change', { target: { value: "foo" }})
    component.find('input').simulate('blur')
    expect(callback.called).to.be.true
  })
})