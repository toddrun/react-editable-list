import React from 'react'
import { expect } from 'chai'
import { shallow } from 'enzyme'
import sinon from 'sinon'

import AddItemButton from '../AddItemButton.jsx'

describe ('AddItemButton', () => {
  const callback = sinon.spy()
  const component = shallow(<AddItemButton onAdd={ callback } />)
                                      
  it ('returns a button element', () => {
    expect(component.find('button')).to.have.length(1)
  })
  
  it ('set a "react-editable-list-add-button" className on the button', () => {
    expect(component.find('button').hasClass('react-editable-list-add-button')).to.be.true
  })
  
  it ('sets button text to "add" by default', () => {
    expect(component.find('button').text()).to.eq('add')
  })
  
  it ('allows setting specific text', () => {
    const specific = shallow(<AddItemButton 
                               buttonLabel='create new' 
                               onAdd={callback} 
                               />
                            )
    expect(specific.find('button').text()).to.eq('create new')
  })
  
  it ('calls "onAdd" prop with an identifier when button is clicked', () => {
    component.simulate('click')
    expect(callback.calledWith(sinon.match.string)).to.be.true
  })
})