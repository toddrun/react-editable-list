import React from 'react'
import { expect } from 'chai'
import { shallow } from 'enzyme'
import sinon from 'sinon'

import DeleteItemButton from '../DeleteItemButton.jsx'

describe ('DeleteItemButton', () => {
  const callback = sinon.spy()
  const identifier = 'one'
  const component = shallow(<DeleteItemButton itemIdentifier={identifier} onDeleted={ callback } />)
                                      
  it ('returns a button element', () => {
    expect(component.find('button')).to.have.length(1)
  })
  
  it ('set a "react-editable-list-delete-button" className on the button', () => {
    expect(component.find('button').hasClass('react-editable-list-delete-button')).to.be.true
  })
  
  it ('sets button text to "delete" by default', () => {
    expect(component.find('button').text()).to.eq('delete')
  })
  
  it ('allows setting specific text', () => {
    const specific = shallow(<DeleteItemButton 
                               buttonLabel='remove' 
                               itemIdentifier='a' 
                               onDeleted={callback} 
                               />
                            )
    expect(specific.find('button').text()).to.eq('remove')
  })
  
  it ('calls "onDeleted" prop when button is clicked', () => {
    component.simulate('click')
    expect(callback.calledWith(identifier)).to.be.true
  })
})