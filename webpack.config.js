var webpack = require('webpack');
var path = require('path');

module.exports = {
  mode: 'production',
	entry: [
		path.join(__dirname, 'lib', 'index.js')
	],
  module: {
		rules: [{
			test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				loader: 'babel-loader'
			}
		]
	},
  output: {
    path: path.resolve('dist'),
    filename: 'ReactEditableList.js',
    libraryTarget: 'commonjs2'
  },
}
