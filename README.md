react-editable-list
==
ReactEditableList React component
--

## Background
This component generates a list of editable values, with add and remove 
capabilities. At some point it may support reordering.

## Installation and Usage
`npm install --save-dev react-editable-list`

In your react code:
```
import ReactEditableList from 'react-editable-list'
 . . . 
  onListUpdated(list) {
    console.log("Got updated list:", list)
  }
  
  render() {
    return this.props.list &&
      <ReactEditableList list={this.props.list} onListUpdated={this.onListUpdated} /> 
  }

```
The "onListUpdated" is called each time a list item has been updated or removed. Adding
a new item will not call "onListUpdated".


## Styling
Styling is left up to your imagination and creativity. By default, the layout
is awful:

![unstyled](https://bytebucket.org/toddrun/react-editable-list/raw/da27bafbf2cf361ba2986954249c4ad359ad004c/aux/react-editable-list-unstyled.png)

However with a bit of styling (see below), you can make it very presentable:

![styled](https://bytebucket.org/toddrun/react-editable-list/raw/da27bafbf2cf361ba2986954249c4ad359ad004c/aux/react-editable-list-styled.png)

The above was created by using "add" and "delete" icons, and applying the 
following styles:
```
  .react-editable-list {
    font-size: 14px;
    list-style: none;
    padding: 0;
  }
  .react-editable-list-item-value {
    width: 200px;
    display: inline-block;
    padding-bottom: 5px;
  }
  .react-editable-list-item-value input {
    width: 190px;
  }
  .react-editable-list-delete-button {
    background-image: url(/public/images/icons/delete.png);
    background-repeat: no-repeat;
    background-position: 50% 50%;
    height: 24px;
    width: 24px;
    border: none;
    font-size: 0px;
  }
  .react-editable-list-add-button {
    background-image: url(/public/images/icons/add.png);
    background-repeat: no-repeat;
    background-position: 50% 50%;
    height: 24px;
    width: 24px;
    border: none;
    font-size: 0px;
    margin-left: 200px;
    margin-top: 5px;
  }
```


